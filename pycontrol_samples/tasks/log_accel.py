from pyControl.utility import *

# States and events.

states = ['FACE_A', 'FACE_B', 'FACE_C', 'FACE_D', 'FACE_E', 'FACE_F']

events = ['timer_evt']

initial_state = 'FACE_A' # it doesn't matter where it starts!!!

# Variables.
v.wait_period = 100
v.accel = pyb.Accel()
v.sensitivity = 18
v.sensitivity2 = -18

# Define behaviour.

def get_next_state(x, y, z, current_state):
    
    if z > v.sensitivity and x < v.sensitivity and y < v.sensitivity and x > v.sensitivity2 and y > v.sensitivity2:
	_next_state = 'FACE_A'
    elif z < v.sensitivity2 and x < v.sensitivity and y < v.sensitivity and x > v.sensitivity2 and y > v.sensitivity2:
	_next_state = 'FACE_B'
    elif x > v.sensitivity and z < v.sensitivity and y < v.sensitivity and z > v.sensitivity2 and y > v.sensitivity2:
	_next_state = 'FACE_C'
    elif x < v.sensitivity2 and z < v.sensitivity and y < v.sensitivity and z > v.sensitivity2 and y > v.sensitivity2:
	_next_state = 'FACE_D'
    elif y > v.sensitivity and x < v.sensitivity and z < v.sensitivity and x > v.sensitivity2 and z > v.sensitivity2:
	_next_state = 'FACE_E'
    elif y < v.sensitivity2 and x < v.sensitivity and z < v.sensitivity and x > v.sensitivity2 and z > v.sensitivity2:
	_next_state = 'FACE_F'
    else: # default state
	_next_state = current_state

    return _next_state

def handle_state(state_name, event):
    if event == 'entry':
	set_timer('timer_evt', v.wait_period * ms)
    elif event == 'timer_evt':
	x, y, z = read_values()
	_next_state = get_next_state(x, y, z, state_name)
	if _next_state != state_name:
	    goto(_next_state)
	else:
	    set_timer('timer_evt', v.wait_period * ms)

def read_values():
    x, y, z = v.accel.x(), v.accel.y(), v.accel.z()
    # print("{x}, {y}, {z}".format(x=x, y=y, z=z))
    return x, y, z

def FACE_A(event):
    handle_state('FACE_A', event)

def FACE_B(event):
    handle_state('FACE_B', event)


def FACE_C(event):
    handle_state('FACE_C', event)

def FACE_D(event):
    handle_state('FACE_D', event)

def FACE_E(event):
    handle_state('FACE_E', event)

def FACE_F(event):
    handle_state('FACE_F', event)






	
