from pyControl.utility import *

# States and events.

states = ['LOG_ACCEL']

events = ['timer_evt']

initial_state = 'LOG_ACCEL'

# Variables.

v.wait_period = 1000
v.accel = pyb.Accel()


# Define behaviour.


def LOG_ACCEL(event):
    if event == 'entry':
	set_timer('timer_evt', v.wait_period * ms)
    elif event == 'timer_evt':
	coords = "{0} {1} {2} ".format(v.accel.x(), v.accel.y(), v.accel.z())
	print(coords)
	set_timer('timer_evt', v.wait_period * ms)
