from pyControl.utility import *
from pyb import udelay, Pin, DAC

tones_codes = {'A1': 70, 'A#': 65, 'B': 60, 'C': 55, 'C#': 50, 'D': 48, 'D#': 45, 'E': 41, 'F': 37, 'F#': 34, 'G': 31, 'G#': 28, 'A2': 25, 'A2#': 22, 'B2': 20}

# States and events.

states = ['WAIT_FOR_CENTER', 'WAIT_FOR_RIGHT', 'WAIT_FOR_LEFT']

events = ['high_poke_out', 'left_poke_out', 'right_poke_out', 'wait_left_right_timer']

initial_state = 'WAIT_FOR_CENTER'

# Variables.
v.wait_poke_timeout = 5000  # Period of blinking when on (in ms)
v.left_tone = tones_codes['G']
v.right_tone = tones_codes['C']
# v.poke_led_timeout = 2000

# Aux methods


def play_success():
    hw.speaker.play_tone(tones_codes['E'], dur=50)
    hw.speaker.play_tone(tones_codes['G#'], dur=50)
    hw.speaker.play_tone(tones_codes['B2'], dur=100)


def play_timeout():
    hw.speaker.play_tone(tones_codes['F'], dur=50)
    hw.speaker.play_tone(tones_codes['E'], dur=50)
    hw.speaker.play_tone(tones_codes['D'], dur=100)


# Define behaviour.


def WAIT_FOR_CENTER(event):
    if event == 'entry':
        hw.center_poke.LED.on()
    elif event == 'high_poke_out':
        if random() > 0.500001:
            goto('WAIT_FOR_RIGHT')
        else:
            goto('WAIT_FOR_LEFT')
    elif event == 'exit':
        set_timer('wait_left_right_timer', v.wait_poke_timeout * ms)
        hw.center_poke.LED.off()


def WAIT_FOR_RIGHT(event):
    if event == 'entry':
        hw.right_poke.LED.on()
        hw.speaker.play_tone(v.left_tone)
    elif event == 'right_poke_out':
        play_success()
        goto('WAIT_FOR_CENTER')
    elif event == 'wait_left_right_timer':
        play_timeout()
        goto('WAIT_FOR_CENTER')
    elif event == 'exit':
        hw.right_poke.LED.off()


def WAIT_FOR_LEFT(event):
    if event == 'entry':
        hw.left_poke.LED.on()
        hw.speaker.play_tone(v.right_tone)
    elif event == 'left_poke_out':
        play_success()
        goto('WAIT_FOR_CENTER')
    elif event == 'wait_left_right_timer':
        play_timeout()
        goto('WAIT_FOR_CENTER')
    elif event == 'exit':
        hw.left_poke.LED.off()


def run_end():  # Turn off hardware at end of run.
    hw.debug_led.off()
    hw.center_poke.LED.off()
    hw.right_poke.LED.off()
    hw.left_poke.LED.off()
