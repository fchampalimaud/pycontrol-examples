import pyb
from . import framework as fw
from . import hardware as hw
from .hardware import Digital_input
from .hardware import Digital_output
from .hardware import Speaker

# ----------------------------------------------------------------------------------------
# Board pin mapping dictionaries.
# ----------------------------------------------------------------------------------------

# These dictionaries provide pin mappings for specific boards whose schematics are
# provided in the pyControl/schematics folder.

breakout_1_0 = {'ports': {1: {'DIO_A': 'X1',   # RJ45 connector port pin mappings.
                              'DIO_B': 'X2',
                              'POW_A': 'Y4',
                              'POW_B': 'Y8'},

                          2: {'DIO_A': 'X3',
                              'DIO_B': 'X4',
                              'POW_A': 'Y3',
                              'POW_B': 'Y7'},

                          3: {'DIO_A': 'X7',
                              'DIO_B': 'X8',
                              'POW_A': 'Y2',
                              'POW_B': 'Y6'},

                          4: {'DIO_A': 'X12',
                              'DIO_B': 'X11',
                              'POW_A': 'Y1',
                              'POW_B': 'Y5'}},
                'BNC_1': 'Y11',      # BNC connector pins.
                'BNC_2': 'Y12',
                'DAC_1': 'X5',
                'DAC_2': 'X6',
                'button_1': 'X9',    # User pushbuttons.
                'button_2': 'X10'}

# ----------------------------------------------------------------------------------------
# Hardware collections.
# ----------------------------------------------------------------------------------------


class Poke(hw.Hardware_group):

    def __init__(self, port, rising=None, falling=None, rising_B=None,
                 falling_B=None, debounce=5, pull=pyb.Pin.PULL_NONE):

        self.SOL = Digital_output(port['POW_A'])
        self.LED = Digital_output(port['POW_B'])
        self.input_A = Digital_input(port['DIO_A'], rising, falling, debounce, pull)
        self.input_B = Digital_input(port['DIO_B'], rising_B, falling_B, debounce, pull)

        self.all_inputs = [self.input_A, self.input_B]
        self.all_outputs = [self.SOL, self.LED]

    def value(self):
        # Return the state of input A.
        return self.input_A.value()


class Box(hw.Hardware_group):

    def __init__(self):

        # Settings for breakout board 1_0
        ports = breakout_1_0['ports']
        pull = pyb.Pin.PULL_NONE

        # Instantiate components.

        self.debug_led = pyb.LED(1)

        self.left_poke = Poke(ports[1], rising='left_poke', falling='left_poke_out',
                              rising_B='session_startstop', pull=pull)
        self.center_poke = Poke(ports[2], rising='high_poke', falling='high_poke_out',
                                rising_B='low_poke', falling_B='low_poke_out', pull=pull)
        self.right_poke = Poke(ports[3], rising='right_poke', falling='right_poke_out', pull=pull)

        self.houselight = self.center_poke.SOL

        self.opto_stim = Digital_output('X12')

        self.house_red = Digital_output('Y1')

        self.speaker = Speaker(breakout_1_0['DAC_1'])

        self.all_inputs = [self.left_poke, self.center_poke, self.right_poke]
        self.all_outputs = [self.left_poke, self.center_poke, self.right_poke, self.opto_stim,
                            self.house_red]
