## Experiment name : FoxP1
## Setup ID : m222
## Start date : 2016/05/25 17:48:32:083385
## Board ID : board1
## Board serial port : /dev/tty.usbmodem1462
## Task name : three_pokes
## Subjects : ['m222']

Read events:
? E {'left_poke_out': 5, 'right_poke_out': 6, 'wait_left_right_timer': 7, 'high_poke_out': 4}

Read states:
? S {'WAIT_FOR_LEFT': 3, 'WAIT_FOR_CENTER': 1, 'WAIT_FOR_RIGHT': 2}

OUTPUT:
? D 0 0 1
? D 15707 0 2
? D 20707 0 1
? D 22329 0 6
? D 27231 0 3
? D 32231 0 1
? D 37778 0 3
? D 42778 0 1
? D 66319 0 3
? D 71319 0 1


## End date : 2016/05/25 17:50:08:684659
