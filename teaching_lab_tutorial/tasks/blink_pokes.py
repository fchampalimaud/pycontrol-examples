from pyControl.utility import *

# States and events.

states = ['LED_on', 'LED_off']

events = ['timer_evt']

initial_state = 'LED_on'

# Variables.

v.LED_n = 1  # Number of LED to use.
v.period_on = 500  # Period of blinking when on (in ms)
v.period_off = 1500  # Period of blinking when off (in ms)

# Define behaviour.


def LED_on(event):
    if event == 'entry':
        set_timer('timer_evt', v.period_on * ms)
        pyb.LED(v.LED_n).on()
        hw.left_poke.LED.on()
        hw.center_poke.LED.on()
        hw.right_poke.LED.on()
    elif event == 'timer_evt':
        goto('LED_off')


def LED_off(event):
    if event == 'entry':
        pyb.LED(v.LED_n).off()
        hw.left_poke.LED.off()
        hw.center_poke.LED.off()
        hw.right_poke.LED.off()
        set_timer('timer_evt', v.period_off * ms)
    elif event == 'timer_evt':
        goto('LED_on')


def run_end():  # Turn off hardware at end of run.
    pyb.LED(v.LED_n).off()
    hw.left_poke.LED.off()
    hw.center_poke.LED.off()
    hw.right_poke.LED.off()
