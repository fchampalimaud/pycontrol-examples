from pyControl.utility import *

# States and events.

states = ['mouse_poke_in', 'mouse_poke_out']

events = ['left_poke', 'left_poke_out']

initial_state = 'mouse_poke_out'

v.my_var = 5000

# Define behaviour.


def mouse_poke_out(event):
    if event == 'entry':
        hw.left_poke.LED.off()
    elif event == 'left_poke':
        goto('mouse_poke_in')


def mouse_poke_in(event):
    if event == 'entry':
        hw.left_poke.LED.on()
    elif event == 'left_poke_out':
        goto('mouse_poke_out')


def run_end():  # Turn off hardware at end of run.
    hw.left_poke.LED.off()
