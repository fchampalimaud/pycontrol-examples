import pyb
from . import framework as fw
from . import hardware as hw
from .hardware import Digital_input
from .hardware import Digital_output
from .hardware import Speaker

# ----------------------------------------------------------------------------------------
# Board pin mapping dictionaries.
# ----------------------------------------------------------------------------------------

# These dictionaries provide pin mappings for specific boards whose schematics are
# provided in the pyControl/schematics folder.

breakout_1_0 = {'ports': {1: {'DIO_A': 'X1',   # RJ45 connector port pin mappings.
                              'DIO_B': 'X2',
                              'POW_A': 'Y4',
                              'POW_B': 'Y8'},

                          2: {'DIO_A': 'X3',
                              'DIO_B': 'X4',
                              'POW_A': 'Y3',
                              'POW_B': 'Y7'},

                          3: {'DIO_A': 'X7',
                              'DIO_B': 'X8',
                              'POW_A': 'Y2',
                              'POW_B': 'Y6'},

                          4: {'DIO_A': 'X12',
                              'DIO_B': 'X11',
                              'POW_A': 'Y1',
                              'POW_B': 'Y5'}},
                'BNC_1': 'Y11',      # BNC connector pins.
                'BNC_2': 'Y12',
                'DAC_1': 'X5',
                'DAC_2': 'X6',
                'button_1': 'X9',    # User pushbuttons.
                'button_2': 'X10'}

# ----------------------------------------------------------------------------------------
# Hardware collections.
# ----------------------------------------------------------------------------------------

class Box(hw.Hardware_group):

    def __init__(self):

        # Settings for breakout board 1_0
        pull = pyb.Pin.PULL_NONE
        debounce = 5

        # Instantiate components.

        self.debug_led = pyb.LED(1)

        self.adc =  Digital_input(breakout_1_0['BNC_1'], 'wave_high', 'wave_low', debounce, pull)

        self.dac = pyb.DAC(pyb.Pin(breakout_1_0['DAC_1']))

        self.all_inputs = [self.adc]
        self.all_outputs = [self.dac]
