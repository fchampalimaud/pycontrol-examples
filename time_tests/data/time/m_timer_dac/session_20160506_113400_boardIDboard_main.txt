## Experiment name : time
## Setup ID : m_timer_dac
## Start date : 2016/05/06 11:34:00:367803
## Board ID : board_main
## Board serial port : /dev/tty.usbmodem1472
## Task name : timer_dac_gui
## Subjects : ['m_timer_dac']

Read events:
? E {'timer_evt': 3}

Read states:
? S {'square_high': 1, 'square_low': 2}

OUTPUT:
