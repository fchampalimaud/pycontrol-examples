from pyControl.utility import *

# States and events.

states = ['WAVE_HIGH', 'WAVE_LOW']

events = ['wave_high', 'wave_low']

initial_state = 'WAVE_HIGH'

# Variables.
        
# Define behaviour.

def WAVE_HIGH(event):
#    if event == 'entry':
#        pyb.LED(v.LED_n).on()
    if event == 'wave_high':
        #hw.debug_led.on()
        hw.dac.write(255)
        goto('WAVE_LOW')

def WAVE_LOW(event):
    if event == 'wave_low':
        #hw.debug_led.off()
        hw.dac.write(0)
        goto('WAVE_HIGH')

def run_end():  # Turn off hardware at end of run.
    hw.off()



