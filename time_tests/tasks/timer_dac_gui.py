from pyControl.utility import *

# States and events.

states = ['square_high', 'square_low']

events = ['timer_evt']

initial_state = 'square_high'

# Variables.

v.period_on = 10  # Period of blinking when on (in ms)
v.period_off = 10  # Period of blinking when off (in ms)

# Define behaviour.


def square_high(event):
    if event == 'entry':
	hw.dac.write(255)
        set_timer('timer_evt', v.period_on)
    elif event == 'timer_evt':
        goto('square_low')


def square_low(event):
    if event == 'entry':
        hw.dac.write(0)
        set_timer('timer_evt', v.period_off)
    elif event == 'timer_evt':
        goto('square_high')